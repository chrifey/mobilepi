#!/bin/bash

## Install packages
apt-get -y update && apt-get -y install vim usbmount ntfs-3g tftpd-hpa hostapd screen nginx udhcpd bash-completion iptables unzip ftpd

apt-get -y remove rpimonitor
rm -f /etc/update-motd.d/*

echo "setting hostname..."
hostnamectl set-hostname mobilepi

echo "setting up environment"
timedatectl set-timezone Europe/Berlin
rm -rf ~/mjpg-streamer
rm /etc/rc.local
ln -s ~/mobilepi/etc/rc.local /etc
chmod 755 /etc/rc.local

rm ~/.bash_profile
ln -f -s ~/mobilepi/.bash_profile ~/.bash_profile
mkdir -p /ontap_download

echo "configuring usbmount..."
mv /etc/usbmount/usbmount.conf /etc/usbmount/usbmount.conf.old
ln -f -s ~/mobilepi/etc/usbmount/usbmount.conf /etc/usbmount/usbmount.conf
mkdir -p /stick

echo "configuring screen..."
ln -f -s ~/mobilepi/etc/udev/rules.d/91-usb-naming.rules /etc/udev/rules.d/
mkdir -p ~/screenlogs

echo "configuring udhcpd..."
mv /etc/default/udhcpd /etc/default/udhcpd.old
mv /etc/udhcpd.conf /etc/udhcpd.conf.old
touch /var/lib/misc/udhcpd.leases
ln -f -s ~/mobilepi/etc/default/udhcpd /etc/default/
ln -f -s ~/mobilepi/etc/udhcpd.conf /etc/udhcpd.conf
systemctl enable udhcpd

echo "configuring hostapd..."
if [ ! -f /root/.custom-hostapd ]
then
  mv /etc/default/hostapd /etc/default/hostapd.old
  ln -s ~/mobilepi/etc/hostapd/hostapd.conf /etc/hostapd/hostapd.conf
  ln -s ~/mobilepi/etc/default/hostapd /etc/default/
  touch ~/.custom-hostapd
  systemctl enable hostapd
fi

echo "configuring network..."
systemctl stop NetworkManager.service
systemctl disable NetworkManager.service
mv /etc/network/interfaces /etc/network/interfaces.old
ln -f -s ~/mobilepi/etc/network/interfaces /etc/network/interfaces
ln -f -s ~/mobilepi/etc/iptables.ipv4.nat /etc

ln -f -s ~/mobilepi/etc/sysctl.d/11-mobilepi.conf /etc/sysctl.d/
rm -f /etc/udev/rules.d/70-persistent-net.rules
ln -f -s ~/mobilepi/etc/udev/rules.d/70-persistent-net.rules /etc/udev/rules.d/

echo "configuring nginx..."
rm -f /etc/nginx/sites-available/default
ln -f -s ~/mobilepi/etc/nginx/sites-available/default /etc/nginx/sites-available/
ln -f -s /stick /var/www/stick
ln -f -s /ontap_download /var/www/ontap

echo "configuring tftpd..."
rm -f /etc/default/tftpd-hpa
ln -f -s ~/mobilepi/etc/default/tftpd-hpa /etc/default/

echo "configuring users..."
grep user /etc/passwd || useradd user 

echo "configuring menu.sh..."
chmod 755 ~/mobilepi/toggle_powersave.sh
chmod 755 ~/mobilepi/set_time.sh
chmod 755 ~/mobilepi/configure_dhcp_ip_eth0.sh
chmod 755 ~/mobilepi/configure_static_ip_eth0.sh
chmod 755 ~/mobilepi/configure_temp_ip_eth0.sh
chmod 755 ~/mobilepi/download_firmware.pl
chmod 755 ~/mobilepi/download_ontap.pl
chmod 755 ~/mobilepi/temp_monitor.sh
if [ ! -f /root/login.cfg ]
then
    cp ~/mobilepi/login.cfg /root/login.cfg
fi
ln -f -s ~/mobilepi/menu.sh /usr/local/bin/
chmod 755 /usr/local/bin/menu.sh
echo "####################################################################"
echo ""
echo "Please place your credentials to /root/login.cfg to use downloader"
echo ""
echo "Please change the password of the root-user with 'passwd root'"
echo ""
echo "Please change the password of the ftp user with 'passwd user'"
echo ""
echo "####################################################################"



