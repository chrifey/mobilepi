#!/bin/sh
TEMP=$(( $(cat /sys/class/thermal/thermal_zone0/temp)/1000 ))
# How hot will we allow the SoC to get?
MAX="78"

while true; do
if [ "${TEMP}" -gt "${MAX}" ] ; then
 # This will be mailed to root if called from cron
 echo "${TEMP}ºC is too hot!"
 # Send a message to syslog
 /usr/bin/logger "Shutting down due to SoC temp ${TEMP}."
 # Halt the box
/sbin/shutdown -h now
fi
sleep 600
done
