#!/bin/bash
echo "Stopping dhcpd and trying to aquire dhcp ip on eth0"
killall udhcpd
systemctl restart udhcpd
ifconfig eth0 0
dhclient eth0
systemctl restart tftpd-hpa
