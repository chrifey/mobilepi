#!/bin/bash
echo "Setting 10.10.10.1 for eth0 and activating dhcpd"
ifconfig eth0 10.10.10.1/24
udhcpd -S ~/mobilepi/etc/udhcpd_eth0.conf
systemctl restart tftpd-hpa