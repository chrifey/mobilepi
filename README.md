## Overview

```
███╗   ███╗ ██████╗ ██████╗ ██╗██╗     ███████╗██████╗ ██╗    ██╗   ██╗██████╗ 
████╗ ████║██╔═══██╗██╔══██╗██║██║     ██╔════╝██╔══██╗██║    ██║   ██║╚════██╗
██╔████╔██║██║   ██║██████╔╝██║██║     █████╗  ██████╔╝██║    ██║   ██║ █████╔╝
██║╚██╔╝██║██║   ██║██╔══██╗██║██║     ██╔══╝  ██╔═══╝ ██║    ╚██╗ ██╔╝██╔═══╝ 
██║ ╚═╝ ██║╚██████╔╝██████╔╝██║███████╗███████╗██║     ██║     ╚████╔╝ ███████╗
╚═╝     ╚═╝ ╚═════╝ ╚═════╝ ╚═╝╚══════╝╚══════╝╚═╝     ╚═╝      ╚═══╝  ╚══════╝
```

This project aims to setup a small NanoPI Neo device as a multi purpose serial console server.

## Hardware

1. NanoPi NEO 512GB [http://wiki.friendlyarm.com/wiki/index.php/NanoPi_NEO](FiendlyARM Wiki)
2. Digitus-DA-70217-Hub-4-Port
3. EDIMAX-EW-7811UN-Wireless-Adapter
4. Micro-SD Card

## 3D-printed Case

[http://www.thingiverse.com/thing:2167434](http://www.thingiverse.com/thing:2167434)

## Soldering the USB Hub

* [Ports NanoPi](http://www.directupload.net/file/d/4656/rrpo9awv_jpg.htm)
* [Ports Data Hub](http://www.directupload.net/file/d/4656/vzb7erii_jpg.htm)
* [Ports Power Hub](http://www.directupload.net/file/d/4656/4c29dheg_jpg.htm)

Red = 5V

Black = GND

Green = D+

White = D-

## Installation

#### Download Armbian image from the following link, write it to your sd-card and complete the setup wizard they provide:

[Armbian Download Page](https://www.armbian.com/nanopi-neo)

[https://armbian.tnahosting.net/dl/nanopineo/archive/Armbian_20.11_Nanopineo_bionic_current_5.8.16.img.xz](https://armbian.tnahosting.net/dl/nanopineo/archive/Armbian_20.11_Nanopineo_bionic_current_5.8.16.img.xz)

#### Login to your NanoPi, install git and clone this repo:

**Note:** If not already done, change the character set of your terminal (e.g. PuTTY) to UTF-8.

**Note:** The default password for the root user is "dietpi".

```
apt-get update && apt-get -y install git
cd
git clone https://gitlab.com/chrifey/mobilepi.git
```

#### Make the setup script executable and run it

```
cd ~/mobilepi
chmod 755 setup.sh
./setup.sh
```

## Usage

#### resize root filesystem (only needed if you are using the "internal" image)

If you have written the prebuild image (this is internal) I created, you need to resize the root filesystem after writing it. This has to be done in two steps and you need to reboot after Step 1.

Step 1 (resize partition, copy and paste the following code):

```
cat << _EOF_ | fdisk /dev/mmcblk0
p
d
2
n
p
2
$(parted /dev/mmcblk0 -ms unit s p | grep ':ext4::;' | sed 's/:/ /g' | sed 's/s//g' | awk '{ print $2 }')

p
w

_EOF_
reboot
```

Step 2 (resize root filesystem):
```
resize2fs /dev/mmcblk0p2
```

#### menu.sh

The idea is, that you start "menu.sh" and with that do the most common tasks like starting a Screen session, downloading firmware or powering down the system

### info

You can type "info" to display the this README.md file

#### nginx

There is a Webserver (nginx) running by default to be able to e.g. download ONTAP images or firmware. If you plug-in a USB stick you can access the data of the first partition via http://172.31.0.1/stick.
If you want to access the local storage that contains the downlaoded firmware the link is http://172.31.0.1/ontap

#### ftp + tftp

There is a tftp server. The directory to put files is /var/lib/tftpboot.

You can also use ftp to access the filesystem. Use the "user" account with the password you have set during installation.

#### COM-Ports

In order to use the serial screen session you need to plug in the Serial2USB adaptors to the two USB ports at the front (near the RJ45 Port).
The adaptor next to the RJ45 Port is COM1 (left side of screen session window) and the adaptor next to the fron USB port is COM3 (right side of the screen session window).

#### Custom hostap config

You can edit the /etc/hostap/hostapd.conf file and adjust the SSID and password as needed. The setup will not overwrite it, after hostapd ist installed. The default SSID is "mobilepiv2" and the default password is "netapp01"
