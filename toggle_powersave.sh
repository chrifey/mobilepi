#!/bin/bash
state=$(cat /sys/devices/system/cpu/cpu2/online)
if [ $state == 0 ]
    then
        echo 1 > /sys/devices/system/cpu/cpu1/online
        echo 1 > /sys/devices/system/cpu/cpu2/online
        echo 1 > /sys/devices/system/cpu/cpu3/online
        echo "active mode is normal"
    else
        echo 0 > /sys/devices/system/cpu/cpu1/online
        echo 0 > /sys/devices/system/cpu/cpu2/online
        echo 0 > /sys/devices/system/cpu/cpu3/online
        echo "active mode is powersave"
fi
