#!/bin/bash

function show_main_title {
  # Set a foreground colour using ANSI escape
  tput setaf 3

  echo "
 ███╗   ███╗ ██████╗ ██████╗ ██╗██╗     ███████╗██████╗ ██╗    ██╗   ██╗██████╗ 
 ████╗ ████║██╔═══██╗██╔══██╗██║██║     ██╔════╝██╔══██╗██║    ██║   ██║╚════██╗
 ██╔████╔██║██║   ██║██████╔╝██║██║     █████╗  ██████╔╝██║    ██║   ██║ █████╔╝
 ██║╚██╔╝██║██║   ██║██╔══██╗██║██║     ██╔══╝  ██╔═══╝ ██║    ╚██╗ ██╔╝██╔═══╝ 
 ██║ ╚═╝ ██║╚██████╔╝██████╔╝██║███████╗███████╗██║     ██║     ╚████╔╝ ███████╗
 ╚═╝     ╚═╝ ╚═════╝ ╚═════╝ ╚═╝╚══════╝╚══════╝╚═╝     ╚═╝      ╚═══╝  ╚══════╝
                                                                                                                                "

  tput sgr0
}

function show_main_menu {
  # clear the screen
  tput clear

  # Move cursor to screen location X,Y (top left is 0,0)
  tput cup 1 15

  show_main_title

  tput cup 10 15
  # Set reverse video mode
  tput rev
  echo "WHAT DO YOU WANT TO DO?"
  tput sgr0

  tput cup 12 15
  echo "1.  Start screen for New FAS / ATTO (115200)"

  tput cup 13 15
  echo "2.  Start screen for NetApp / Switch (9600)"

  tput cup 14 15
  echo "3.  Download latest NetApp firmware (needs internet and login.cfg set)"

  tput cup 15 15
  echo "4.  Download specific ONTAP Version (needs internet and login.cfg set)"

  tput cup 16 15
  echo "5.  Configure static IP and DHCP-server for RJ45 Port"

  tput cup 17 15
  echo "6.  Configure DHCP client for RJ45 Port"

  tput cup 18 15
  echo "7.  Configure temp IP for RJ45 Port"

  tput cup 19 15
  echo "8.  Toggle powersave mode"

  tput cup 20 15
  echo "9.  Set time"

  tput cup 21 15
  echo "10. Poweroff system"

  tput cup 22 15
  echo "11. I want to quit this menu"

  # Set bold mode
  tput bold
  tput cup 24 15

  # Get entered number in choice variable
  read -p "Enter your choice [1-11] " choice

  tput clear
  tput sgr0

  case "$choice" in
    "1") show_action_menu "Starting Screen..." "screen_atto"
         ;;
    "2") show_action_menu "Starting Screen..." "screen_netapp"
         ;;
    "3") show_action_menu "Downloading latest firmware. PLEASE WAIT..." "download_firmware"
         ;;
    "4") show_action_menu "Starting ONTAP Downloader..." "download_ontap"
         ;;
    "5") show_action_menu "Configuring static IP for RJ45..." "configure_static_ip_eth0"
         ;;
    "6") show_action_menu "Configuring DHCP client for RJ45..." "configure_dhcp_ip_eth0"
         ;;
    "7") show_action_menu "Configuring temp IP for RJ45..." "configure_temp_ip_eth0"
         ;;
    "8") show_action_menu "Toggle powersave mode..." "toggle_powersave"
         ;;
    "9") show_action_menu "Setting the time..." "set_time"
         ;;
    "10") show_action_menu "Powering off..." "power_off"
         ;;
    "11") echo "Exit..."
         exit 0
         ;;
    *) echo "Wrong choice... Please try again."
       show_main_menu
       ;;
  esac
}

function show_action_menu {
  # $1 = Action title
  # $2 = Action name
  tput clear

  tput cup 1 15

  show_main_title

  tput cup 10 15

  tput rev
  echo $1
  tput sgr0

  tput bold
  tput cup 12 15

  run_task $2
}

function run_task {
  echo 'Task start...'

  tput sgr0

  task_name=$1

  case "$task_name" in
    "screen_atto")
      # Change the line below to your action
      screen -c ~/mobilepi/screenrc-atto
      ;;
    "screen_netapp")
      screen -c ~/mobilepi/screenrc-ntap
      ;;
    "download_firmware")
      /root/mobilepi/download_firmware.pl
      ;;
    "set_time")
      /root/mobilepi/set_time.sh
      ;;
    "download_ontap")
      read -p "Enter Version to download e.g. 9.1P1: " version
      /root/mobilepi/download_ontap.pl $version
      ;;
    "configure_static_ip_eth0")
      /root/mobilepi/configure_static_ip_eth0.sh
      ;;
    "configure_dhcp_ip_eth0")
      /root/mobilepi/configure_dhcp_ip_eth0.sh
      ;;
    "configure_temp_ip_eth0")
      /root/mobilepi/configure_temp_ip_eth0.sh
      ;;
    "toggle_powersave")
      /root/mobilepi/toggle_powersave.sh
      ;;
    "power_off")
      echo "none" > /sys/class/leds/blue_led/trigger; init 0
      ;;
    *) echo "Error. Action '$task_name' doesn't exist."
       exit 0
       ;;
  esac

  # Calculate current position of the cursor
  extract_current_cursor_position last_position
  last_line_number=${last_position[0]}

  tput cup $[$last_line_number + 1] 15

  tput bold
  echo 'Task end.'
}

#############################
#  Helpers  #
#############################

# http://askubuntu.com/questions/366103/saving-more-corsor-positions-with-tput-in-bash-terminal
function extract_current_cursor_position {
    export $1
    exec < /dev/tty
    oldstty=$(stty -g)
    stty raw -echo min 0
    echo -en "\033[6n" > /dev/tty
    IFS=';' read -r -d R -a pos
    stty $oldstty
    eval "$1[0]=$((${pos[0]:2} - 2))"
    eval "$1[1]=$((${pos[1]} - 1))"
}

# Start point
show_main_menu
