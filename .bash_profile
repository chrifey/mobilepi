#!/bin/bash
echo $(tput setaf 2)"                                                                               "
echo "███╗   ███╗ ██████╗ ██████╗ ██╗██╗     ███████╗██████╗ ██╗    ██╗   ██╗██████╗ "
echo "████╗ ████║██╔═══██╗██╔══██╗██║██║     ██╔════╝██╔══██╗██║    ██║   ██║╚════██╗"
echo "██╔████╔██║██║   ██║██████╔╝██║██║     █████╗  ██████╔╝██║    ██║   ██║ █████╔╝"
echo "██║╚██╔╝██║██║   ██║██╔══██╗██║██║     ██╔══╝  ██╔═══╝ ██║    ╚██╗ ██╔╝██╔═══╝ "
echo "██║ ╚═╝ ██║╚██████╔╝██████╔╝██║███████╗███████╗██║     ██║     ╚████╔╝ ███████╗"
echo "╚═╝     ╚═╝ ╚═════╝ ╚═════╝ ╚═╝╚══════╝╚══════╝╚═╝     ╚═╝      ╚═══╝  ╚══════╝"
echo $(tput sgr0)"                                                                               "
let upSeconds="$(/usr/bin/cut -d. -f1 /proc/uptime)"
let secs=$((${upSeconds}%60))
let mins=$((${upSeconds}/60%60))
let hours=$((${upSeconds}/3600%24))
let days=$((${upSeconds}/86400))
UPTIME=`printf "%d days, %02dh%02dm%02ds" "$days" "$hours" "$mins" "$secs"`

# get the load averages
read one five fifteen rest < /proc/loadavg

echo "$(tput setaf 2)
`date +"%A, %e %B %Y, %r"`
`uname -srmo`$(tput setaf 1)
Uptime.............: ${UPTIME}
Memory.............: `cat /proc/meminfo | grep MemFree | awk {'print $2'}`kB (Free) / `cat /proc/meminfo | grep MemTotal | awk {'print $2'}`kB (Total)
Load Averages......: ${one}, ${five}, ${fifteen} (1, 5, 15 min)
Running Processes..: `ps ax | wc -l | tr -d " "`
IP Addresses.......: wlan0 `/sbin/ifconfig wlan0 | grep "inet "| awk '{ print $2}'` and eth0 `/sbin/ifconfig eth0 | grep "inet " | awk '{ print $2}'`
CPU-Temperature....: $(( $(cat /sys/class/thermal/thermal_zone0/temp)/1000 ))°
$(tput sgr0)"
alias ll='ls -la'
alias info='less ~/mobilepi/README.md'
